<?php
/*
Plugin Name: Vimeo Lightbox Widget
Description: Pulls a thumbnail and links to a video.  Also can use shortcode.
Author: Corey Taira
Version: 1.2
Plugin URI: http://webheadcoder.com/
Author URI: http://webheadcoder.com/
*/
 

class Vimeo_Widget extends WP_Widget {
  function Vimeo_Widget() {
    $widget_ops = array('classname' => 'Vimeo_Widget', 'description' => 'Display a thumbnail of the vimeo vidoe and pops up the video.');
    $this->WP_Widget('Vimeo_Widget', 'Vimeo Widget', $widget_ops);
  }
 
  function form($instance) {
    $instance = wp_parse_args((array) $instance, array( 'title' => '' , 'vimeo_id' => ''));
    $title = $instance['title'];
    $vimeo_id = $instance['vimeo_id'];
    $rel = $instance['rel'];
    $link_class = $instance['link_class'];
?>
  <p>
    <label>Requires a images/video.png in your theme.</label>
  </p>
  <p>
      <label for="<?php echo $this->get_field_id('title'); ?>">
          Title: <input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                     name="<?php echo $this->get_field_name('title'); ?>"
                     value="<?php echo attribute_escape($title); ?>" />
      </label>
  </p>

  <p>
      <label for="<?php echo $this->get_field_id('vimeo_id'); ?>">
          Video ID: <input type="text" class="widefat" id="<?php echo $this->get_field_id('vimeo_id'); ?>"
                        name="<?php echo $this->get_field_name('vimeo_id'); ?>"
                        value="<?php echo attribute_escape($vimeo_id); ?>" />
      </label>
  </p>
    <p>
      <label for="<?php echo $this->get_field_id('link_class'); ?>">
          class: <input type="text" class="widefat" id="<?php echo $this->get_field_id('link_class'); ?>"
                        name="<?php echo $this->get_field_name('link_class'); ?>"
                        value="<?php echo attribute_escape($link_class); ?>" />
      </label>
  </p>  
  
    <p>
      <label for="<?php echo $this->get_field_id('rel'); ?>">
          rel attr (for lightbox): <input type="text" class="widefat" id="<?php echo $this->get_field_id('rel'); ?>"
                        name="<?php echo $this->get_field_name('rel'); ?>"
                        value="<?php echo attribute_escape($rel); ?>" />
      </label>
  </p>
<?php
  }
 
  function update($new_instance, $old_instance) {
    $instance = $old_instance;
    $instance['title'] = $new_instance['title'];
    $instance['vimeo_id'] = $new_instance['vimeo_id'];
    $instance['rel'] = $new_instance['rel'];
    $instance['link_class'] = $new_instance['link_class'];
    return $instance;
  }
 
  function widget($args, $instance) {
    extract($args, EXTR_SKIP);
    wp_enqueue_style( 'vw_style', WP_PLUGIN_URL . '/video-lb-widget/vw_style.css');
 
    echo $before_widget;
    $title = empty($instance['title']) ? '' : apply_filters('widget_title', $instance['title']);
    $vimeo_id = empty($instance['vimeo_id']) ? 0 : intval($instance['vimeo_id']);
    $rel = empty($instance['rel']) ? '' : attribute_escape($instance['rel']);
    $link_class = empty($instance['link_class']) ? '' : attribute_escape($instance['link_class']);
 
    if (!empty($title))
      echo $before_title . $title . $after_title;;
 
    echo vlb_getHtml($vimeo_id, $rel, $title, $link_class);
    echo $after_widget;
  }
}
add_action( 'widgets_init', create_function('', 'return register_widget("Vimeo_Widget");') );


/**
 * Returns the HTML necessary to create the popup.
 */
function vlb_getHtml($vimeo_id='', $rel='', $title='', $link_class='') {
	$hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$vimeo_id.php"));
	$thumb =  $hash[0]['thumbnail_medium'];
	$ret = '';
	if (strlen($thumb) == 0) {
		$ret .= "<img src=\"" . get_bloginfo('template_directory') . "/images/video.png\">";
	}
	else {
		$ret .= "<a title=\"" . $title . "\" class=\"" .  $link_class . "\" style=\"position:relative;z-index:800;\"  href=\"http://vimeo.com/" . $vimeo_id . "?width=640&amp;height=480\" rel=\"" . $rel . "\" title=\"\">
		  <img class=\"video-widget-frame\" src=\"" .  get_bloginfo('template_directory') . "/images/video.png\" alt=\"Vimeo\" border=\"0\"></a>";
		$ret .= "<img class='video-widget' src=\"" . $thumb . "\">";
	}
	return $ret;
}

/**
 * Shortcode to include vimeo thumb image with arrow overlay.  Similar to widget.  Uses wp-video-ligthbox shortcode.
 * [vimeothumb video_id=XXX width=WWW height=HHH anchor=http://link.to.override.vimeo rel=RRR]
 */
function vimeo_lb_shortcode($atts) {
    
	extract( shortcode_atts( array(
		'video_id' => '',
		'rel'    => 'wp-video-lightbox'
	), $atts ) );
	
	$ret = '';
	$ret .= '<div class="vlb-shortcode-container">';
	$ret .= vlb_getHtml($video_id, $rel);
	$ret .= '</div>';
	return $ret;
	
	
}
add_shortcode('vimeothumb', 'vimeo_lb_shortcode');
?>