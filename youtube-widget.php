<?php
/*
Plugin Name: YouTube Lightbox Widget
Description: Pulls a thumbnail and links to a video.
Author: Corey Taira
Version: 1.1
Author URI: http://webheadcoder.com/
*/
 

class YouTube_LB_Widget extends WP_Widget {
  function YouTube_LB_Widget() {
    $widget_ops = array('classname' => 'YouTube_LB_Widget', 'description' => 'Display a thumbnail of the youtube video and pops up the video if you have a lightbox plugin.');
    $this->WP_Widget('YouTube_LB_Widget', 'YouTube Widget', $widget_ops);
  }
  
  function form($instance) {
    $instance = wp_parse_args((array) $instance, array( 'title' => '' , 'youtube_id' => ''));
    $title = $instance['title'];
    $youtube_id = $instance['youtube_id'];
    $rel = $instance['rel'];
    $link_class = $instance['link_class'];
?>
  <p>
    <label>Requires a images/video.png in your theme.</label>
  </p>
  <p>
      <label for="<?php echo $this->get_field_id('title'); ?>">
          Title: <input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                     name="<?php echo $this->get_field_name('title'); ?>"
                     value="<?php echo attribute_escape($title); ?>" />
      </label>
  </p>

  <p>
      <label for="<?php echo $this->get_field_id('youtube_id'); ?>">
          Video ID: <input type="text" class="widefat" id="<?php echo $this->get_field_id('youtube_id'); ?>"
                        name="<?php echo $this->get_field_name('youtube_id'); ?>"
                        value="<?php echo attribute_escape($youtube_id); ?>" />
      </label>
  </p>
    <p>
      <label for="<?php echo $this->get_field_id('link_class'); ?>">
          class: <input type="text" class="widefat" id="<?php echo $this->get_field_id('link_class'); ?>"
                        name="<?php echo $this->get_field_name('link_class'); ?>"
                        value="<?php echo attribute_escape($link_class); ?>" />
      </label>
  </p>  
    <p>
      <label for="<?php echo $this->get_field_id('rel'); ?>">
          rel attr (for lightbox): <input type="text" class="widefat" id="<?php echo $this->get_field_id('rel'); ?>"
                        name="<?php echo $this->get_field_name('rel'); ?>"
                        value="<?php echo attribute_escape($rel); ?>" />
      </label>
  </p>
<?php
  }
 
  function update($new_instance, $old_instance) {
    $instance = $old_instance;
    $instance['title'] = $new_instance['title'];
    $instance['youtube_id'] = $new_instance['youtube_id'];
    $instance['rel'] = $new_instance['rel'];
    $instance['link_class'] = $new_instance['link_class'];
    return $instance;
  }
 
  function widget($args, $instance) {
    extract($args, EXTR_SKIP);
    wp_enqueue_style( 'vw_style', WP_PLUGIN_URL . '/video-lb-widget/vw_style.css');
    
    echo $before_widget;
    $title = empty($instance['title']) ? '' : apply_filters('widget_title', $instance['title']);
    $youtube_id = empty($instance['youtube_id']) ? 0 : $instance['youtube_id'];
    $rel = empty($instance['rel']) ? '' : attribute_escape($instance['rel']);
    $link_class = empty($instance['link_class']) ? '' : attribute_escape($instance['link_class']);
 
    if (!empty($title))
      echo $before_title . $title . $after_title;;
    // Do Your Widgety Stuff Here...
    echo "<a title=\"" . $title . "\" class=\"" .  $link_class . "\" style=\"position:relative;z-index:800;\"  href=\"http://www.youtube.com/watch?v=" . $youtube_id . "&rel=0\" rel=\"" . $rel . "\" title=\"\">
      <img class=\"video-widget-frame\" src=\"" .  get_bloginfo('template_directory') . "/images/video.png\" border=\"0\"></a>";
    echo "<img class='video-widget' src=\"http://img.youtube.com/vi/" . $youtube_id . "/0.jpg\">";
    echo $after_widget;
  }
}
add_action( 'widgets_init', create_function('', 'return register_widget("YouTube_LB_Widget");') );
 
?>